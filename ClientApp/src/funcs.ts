export function getColorFromInt(colorInt: number): string {
    var r = (((255 << 16) & colorInt) >> 16).toString(16);
    var g = (((255 << 8) & colorInt) >> 8).toString(16);
    var b = ((255) & colorInt).toString(16);
    var color = "#" + (r.length === 1 ? "0" + r : r) + (g.length === 1 ? "0" + g : g) + (b.length === 1 ? "0" + b : b);
    return color;
}