import { EventEmitter } from "events";

export class SWWebSocket extends EventEmitter {
    websocket: WebSocket;

    createWebsocket() {
        this.websocket = new WebSocket("wss://" + window.location.host + "/websocket");
        this.websocket.onmessage = event => this.handleMessage(event.data);
        return this;
    }

    handleMessage(this: SWWebSocket, data: string) {
        const message = JSON.parse(data);
        switch (message.type) {
            case "method":
                if (message.method === "hello") {
                    this.websocket.send(JSON.stringify({ type: "reply", reply: "hello" }));
                }
                if (message.method === "id") {
                    window.localStorage.setItem("websocketID", message.id);
                }
                if (message.method === "auth") {
                    fetch("api/v1/users/auth", { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify({ id: message.id, name: message.name, discriminator: message.discriminator }) }).then(res => {
                        if (res.ok)
                            return res.json();
                        else
                            return Promise.reject();
                    }).then(res => {
                        console.log(res);
                        window.localStorage.setItem("id", res.id);
                        window.localStorage.setItem("authObj", JSON.stringify(res));
                        this.emit("recheckauth");
                    });
                }
                if(message.method === "checklistupdate") {
                    this.emit("checklistupdate", message.data);
                }
                break;
            case "reply":
                if (message.reply === "auth") {
                    window.close();
                }
                break;
        }
    }

    sendAuth(this: SWWebSocket, data: { id: string, name: string, discriminator: string, state: string }) {
        if (this.websocket.readyState !== WebSocket.OPEN) {
            setTimeout(() => {
                this.sendAuth(data);
            }, 100);
            return;
        }
        this.websocket.send(JSON.stringify({ type: "method", method: "auth", data: data }));
    }

    sendMethod(this: SWWebSocket, method: string, data: any) {
        this.websocket.send(JSON.stringify({ type: "method", method: method, data: data }));
    }

    public on(event: "checklistupdate", cb: (update: {uid: string, grid: string, game: string, completed: boolean}) => void): this;
    public on(event: "recheckauth", cb: () => void): this;
    public on(event: string, cb: any): this{
        return super.on(event, cb);
    }
}