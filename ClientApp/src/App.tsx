import React, { Component } from "react";
import { Route, RouteProps } from "react-router";
import { Layout } from "./components/Layout";
import { SWWebSocket } from "./WebSocketHandler";
import classnames from "classnames";
import { CheckList, Dictionary } from "./components/CheckList";
import { Nav, NavItem, NavLink, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Progress } from "reactstrap";
import { Link } from "react-router-dom";
import * as H from 'history';

import "./styles/Default.scss";

export default class App extends Component {
    static displayName = App.name;
    static webSocket = new SWWebSocket().createWebsocket();

    componentDidMount() {
        (window as any).App = this;
    }

    render() {
        return (
            <Layout>
                <Route exact path="/" component={Main} />
                <Route exact path="/login" component={Login} />
                <Route path="/:id" component={MainGameSelect} />
            </Layout>
        );
    }
}

function getGameCheckListData(inp: string, cb: (res: string) => void) {
    switch (inp) {
        case "xc2":
            return fetch("xc2data.json").then(res => res.json()).then(res => {
                cb(res);
            })
        case "xc":
            return fetch("xcdata.json").then(res => res.json()).then(res => {
                cb(res);
            })
    }
}

function convertGameJson(inp: any): any {
    for (let i = 0; i < inp.length; i++) {
        const element = inp[i];
        if (element.tabs === undefined) element.tabs = {};
        else element.tabs = JSON.parse(element.tabs);
    }
    return inp;
}

function getHash(location: H.Location<{}>): any {
    return location.hash.substring(1).split("&").map(t => {
        var f = t.split("=");
        return JSON.parse('{"' + f[0] + '":"' + f[1] + '"}');
    }).reduce((result, current) => Object.assign(result, current))
}

function modifHash(hash: string, key: string, value: string): string {
    if (hash.length === 0) return "#" + key + "=" + value;
    else if (hash.includes(key))
        return "#" + hash.substring(1).split("&").map(t => {
            var f = t.split("=");
            if (f[0] === key) f[1] = value;
            return f[0] + "=" + f[1];
        }).join("&");
    else {
        return hash + "&" + key + "=" + value;
    }
}

class MainGameSelect extends Component<any, { games: any[], admin: boolean, sel: { tab: string, game: string, first: boolean } }> {
    constructor(props: any) {
        super(props);
        var hash = getHash((props as RouteProps).location);
        this.state = { games: [], admin: false, sel: { tab: hash.tab, game: hash.game, first: true } };
        App.webSocket.on("recheckauth", this.checkAuth.bind(this));
    }

    shouldComponentUpdate(props: any, state: any) {
        var hash = getHash((props as RouteProps).location);
        var f = hash.tab !== this.state.sel.tab || this.state.sel.game !== hash.game || this.state.sel.first || this.state.admin !== state.admin;
        if(hash.tab !== this.state.sel.tab || this.state.sel.game !== hash.game) this.changeSel({game: hash.game, tab: hash.tab});
        return f;
    }

    changeSel(inp: { tab?: string, game?: string, first?: boolean }) {
        this.setState({ sel: (Object.assign(this.state.sel, inp) as { tab: string, game: string, first: boolean }) });
    }

    checkAuth() {
        var id = window.localStorage.getItem("id");
        if (id !== undefined && id !== null)
            fetch("api/v1/users/admin", { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify({ guid: id }) })
                .then(res => {
                    if (res.ok)
                        return res.json();
                    else
                        return Promise.reject();
                }).then(res => {
                    this.setState({ admin: res.b });
                });
        else
            this.setState({ admin: false });
    }

    componentDidUpdate() {
        this.changeSel({first: false});
    }

    componentDidMount() {
        fetch("api/v1/games/get").then(res => {
            if (res.ok)
                return res.json();
            else
                return Promise.reject();
        }).then(res => {
            this.setState({ games: convertGameJson(res) });
            this.checkAuth();
        });
    }

    render() {
        console.log(this.state);
        var location = (this.props as RouteProps).location;
        var pathname = location.pathname.substring(1);
        if (pathname.includes("/login")) return (<div></div>);
        if (this.state.games.length === 0) return (<div></div>);
        return (<div>
            <Nav>
                {this.state.games.map(game => (
                    <NavItem key={game.name}>
                        <NavLink tag={Link} className="text-light" to={modifHash(modifHash(location.hash, "game", game.name.toLowerCase()), "tab", Object.keys(this.state.games.filter(f => f.name.toLowerCase() === game.name.toLowerCase())[0].tabs)[0])}>
                            {game.title}
                        </NavLink>
                    </NavItem>
                ))}
            </Nav>
            {this.state.sel.game !== undefined ? <MainCheckList game={this.state.sel.game} loc={pathname} tabs={this.state.games.filter(f => f.name.toLowerCase() === this.state.sel.game)[0].tabs} admin={this.state.admin} tab={this.state.sel.tab} {...this.props}></MainCheckList> : null}
        </div>)
    }
}

class MainCheckList extends Component<any, { dropdownid: string, dropdownopen: boolean, data: any, sel: { activeTab: string, first: boolean } }> {
    constructor(props: any) {
        super(props);
        var tab = getHash((props as RouteProps).location).tab;
        this.state = { dropdownid: "", dropdownopen: false, data: { xc2: undefined, xc: undefined, xc2load: undefined, xcload: undefined }, sel: { activeTab: tab === undefined ? Object.keys(props.tabs)[0] : tab, first: true } };
        this.toggleDropdown = this.toggleDropdown.bind(this);
        this.updateCheckList = this.updateCheckList.bind(this);
        App.webSocket.on("checklistupdate", this.updateCheckList);
    }

    shouldComponentUpdate(props: any) {
        var tab = getHash((props as RouteProps).location).tab;
        var f = tab !== this.state.sel.activeTab || this.props.game !== props.game || this.state.sel.first || this.props.admin !== props.admin;
        if(tab !== this.state.sel.activeTab) 
            this.changeSel({ activeTab: tab });
        return f;
    }

    UNSAFE_componentWillReceiveProps(props: any) {
        var tab: string = getHash((props as RouteProps).location).tab;
        if (Object.keys(props.tabs).includes(tab)) this.changeSel({activeTab: tab});
        else {
            tab = Object.keys(props.tabs)[0];
            window.location.hash = modifHash(window.location.hash, "tab", tab);
        }
        if (this.state.data[props.game + "load"] > Math.floor(Date.now() / 1000) + 3600 || this.state.data[props.game + "load"] === undefined) {
            this.state.data[props.game + "load"] = Math.floor(Date.now() / 1000);
            getGameCheckListData(props.game, res => {
                this.state.data[props.game] = res;
                this.forceUpdate();
            });
            fetch("api/v1/checklist/get?game=" + props.game + "&loc=" + props.loc).then(res => {
                if (res.ok)
                    return res.json();
                else
                    return Promise.reject();
            }).then((res: CheckListData[]) => {
                res.forEach(data => this.updateCheckList(data, false));
                this.forceUpdate();
            });
        }
    }

    componentDidUpdate() {
        this.changeSel({ first: false });
    }

    componentDidMount() {
        this.state.data[this.props.game + "load"] = Math.floor(Date.now() / 1000);
        getGameCheckListData(this.props.game, res => {
            this.state.data[this.props.game] = res;
            this.forceUpdate();
        });
        fetch("api/v1/checklist/get?game=" + this.props.game + "&loc=" + this.props.loc).then(res => {
            if (res.ok)
                return res.json();
            else
                return Promise.reject();
        }).then((res: CheckListData[]) => {
            res.forEach(data => this.updateCheckList(data, false));
            this.forceUpdate();
        });
    }

    changeSel(inp: { activeTab?: string, first?: boolean }) {
        this.setState({ sel: (Object.assign(this.state.sel, inp) as { activeTab: string, first: boolean }) });
    }

    updateCheckList(update: CheckListData, forceUpdate = true): void {
        var rawjson = window.localStorage.getItem(update.game);
        if (rawjson === undefined || rawjson === null) rawjson = "{}";
        var json = JSON.parse(rawjson);
        if (json[update.grid] === undefined) json[update.grid] = {};
        json[update.grid][update.uid] = update.completed;
        window.localStorage.setItem(update.game, JSON.stringify(json));
        if (forceUpdate) this.forceUpdate();
    }

    toggleTab(inp: string) {
        this.setState({ dropdownopen: false });
        window.location.hash = modifHash(window.location.hash, "tab", inp);
        this.forceUpdate();
    }

    toggleDropdown(inp: string) {
        var nextOpenState = !this.state.dropdownopen;
        if (this.state.dropdownid !== inp) nextOpenState = true;
        this.setState({ dropdownid: inp, dropdownopen: nextOpenState });
        this.forceUpdate();
    }

    render() {
        var tabs: { [id: string]: Tabs } = this.props.tabs;
        var groupTabs = JSON.parse("{}");
        Object.entries(tabs).filter(f => Object.keys(f[1]).includes("group")).forEach(f => {
            if (groupTabs[f[1].group] === undefined) groupTabs[f[1].group] = {};
            groupTabs[f[1].group][f[0]] = f[1];
        });
        var tab = Object.keys(tabs).find(tab => this.state.sel.activeTab === tab);
        var rawjson = window.localStorage.getItem(this.props.game);
        if (rawjson === undefined || rawjson === null) rawjson = "{}";
        var json = JSON.parse(rawjson);
        var items = Object.values(json).flatMap(t => Object.values(t));
        var completed = items.filter(t => t);
        console.log(this.props);
        return (
            <div>
                <br />
                <div className="text-center">{completed.length} of {items.length}</div>
                <Progress value={completed.length} max={items.length}></Progress>
                <br />
                <Nav tabs>
                    {Object.keys(groupTabs).map(gtab => (
                        <NavItem key={gtab}>
                            <NavLink className={classnames({ active: Object.keys(groupTabs[gtab]).includes(this.state.sel.activeTab) })} style={{ padding: 0, paddingBottom: "4px" }}>
                                <Dropdown isOpen={this.state.dropdownopen && this.state.dropdownid === gtab} toggle={() => this.toggleDropdown(gtab)}>
                                    <DropdownToggle caret className={classnames({ selected: Object.keys(groupTabs[gtab]).includes(this.state.sel.activeTab), "btn-#495057": true })}>
                                        {gtab}
                                    </DropdownToggle>
                                    <DropdownMenu>
                                        {Object.keys(groupTabs[gtab]).map(tab => (
                                            <DropdownItem key={gtab + "-" + tab} active={this.state.sel.activeTab === tab} onClick={() => { this.toggleTab(tab) }}>
                                                {JSON.parse(JSON.stringify(tabs))[tab].title}
                                            </DropdownItem>
                                        ))}
                                    </DropdownMenu>
                                </Dropdown>
                            </NavLink>
                        </NavItem>
                    ))}
                    {Object.keys(tabs).map(tab =>
                        JSON.parse(JSON.stringify(tabs))[tab].group ? null :
                            <NavItem key={"ungrouped-" + tab}>
                                <NavLink className={classnames({ active: this.state.sel.activeTab === tab })} onClick={() => { this.toggleTab(tab) }}>
                                    {JSON.parse(JSON.stringify(tabs))[tab].title}
                                </NavLink>
                            </NavItem>
                    )}
                </Nav>
                {
                    this.state.data[this.props.game] !== undefined ? (<CheckList key={tab} data={this.state.data[this.props.game][tab]} grid={tab} game={this.props.game} loc={this.props.loc} keys={new Dictionary(tabs[tab].keys)} admin={this.props.admin}></CheckList>) : null
                }
            </div>
        );
    }
}

interface CheckListData {
    uid: string;
    completed: boolean;
    grid: string;
    game: string;
}

interface Tabs {
    title: string;
    group?: string;
    keys: { key: string, value: string }[];
}

class Main extends Component {
    render() {
        return (<div>
            <h1>Xenoblade Chronicles Check Lists</h1>
            <p>This is made for <a href="https://mixer.com/wildwolf">WildWolf</a> to use as a checklist when going through both Xenoblade Chronicles and Xenoblade Chronicles 2 for a 100% completion of both games. Checkout the progress <a href="/wildwolf">here.</a></p>
        </div>);
    }
}

class Login extends Component {

    componentDidMount() {
        var search = window.location.hash.substr(1).split('&').map(t => t.split('=')).reduce((map: { [key: string]: string }, obj) => { map[obj[0]] = obj[1]; return map }, {});
        if (search["error"] !== undefined) {
            window.close();
        }
        else {
            fetch("https://discordapp.com/api/v6/users/@me", {
                headers: {
                    "Authorization": "Bearer " + search["access_token"]
                }
            }).then(res => {
                if (res.ok)
                    return res.json();
                else
                    return Promise.reject();
            }).then(res => {
                App.webSocket.sendAuth({
                    id: res.id,
                    name: res.username,
                    discriminator: res.discriminator,
                    state: search["state"]
                });
            });
        }
    }

    render() {
        return (
            <div>
            </div>
        );
    }
}