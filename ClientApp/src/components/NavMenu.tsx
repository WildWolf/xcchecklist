import React, { Component } from "react";
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from "reactstrap";
import { Link } from "react-router-dom";
import "../styles/NavMenu.scss";
import App from "../App";

const discordLink = "https://discordapp.com/api/oauth2/authorize?client_id=644415570581651467&redirect_uri=https%3A%2F%2F" + window.location.host + "%2Flogin&response_type=token&scope=identify";

export class NavMenu extends Component<any, { collapsed: boolean, hidden: boolean, admin: boolean }> {
    static displayName = NavMenu.name;

    constructor(props: any) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            collapsed: true,
            hidden: true,
            admin: false
        };
        App.webSocket.on("recheckauth", this.checkAuth.bind(this));
    }

    checkAuth() {
        var id = window.localStorage.getItem("id");
        if (id !== undefined && id !== null)
            fetch("api/v1/users/admin", { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify({ guid: id }) })
                .then(res => {
                    if (res.ok)
                        return res.json();
                    else
                        return Promise.reject();
                }).then(res => {
                    this.setState({ admin: res.b });
                });
        else
            this.setState({ admin: false });
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {
        // <Container>
        //             <h1 className="text-center" style={{ marginBottom: "32px", marginTop: "32px" }}>Sky Wolves</h1>
        //             <h5 className="text-center" style={{ marginBottom: "64px" }}>Suck it up buttercup.</h5>
        //         </Container>
        return (
            window.location.pathname === "/login" ?
                <header></header>
                :
                <header style={{ height: "56px", padding: "0.5rem 1rem", marginBottom: "1rem"}}>
                    <nav style={{ position: "fixed", width: "100%", top: 0, left: 0, zIndex: 100000000 }} className="navbar navbar-expand-sm navbar-toggleable-sm ng-white box-shadow mb-3">
                        <Container>
                            <NavbarToggler onClick={this.toggleNavbar} className="mr-2 navbar-dark" />
                            <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
                                <ul className="navbar-nav flex-grow">
                                    <NavItem>
                                        <NavLink tag={Link} className="text-light" to="/">Home</NavLink>
                                    </NavItem>
                                    {this.state.admin ? <NavItem><NavLink tag={Link} className="text-light" to="#">Admin</NavLink></NavItem> : null}
                                    <NavItem>
                                        <LoginLink />
                                    </NavItem>
                                </ul>
                            </Collapse>
                        </Container>
                    </nav>
                    <Collapse isOpen={!this.state.hidden} style={{ position: "fixed", width: "100%", top: 0, zIndex: 100000000, left: 0 }}>
                        <Navbar className="navbar navbar-expand-sm navbar-toggleable-sm ng-white box-shadow mb-3" dark>
                            <Container>
                                <NavbarBrand tag={Link} to="/">XC Check List</NavbarBrand>
                                <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
                                <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
                                    <ul className="navbar-nav flex-grow">
                                        <NavItem>
                                            <NavLink tag={Link} className="text-light" to="/">Home</NavLink>
                                        </NavItem>
                                        {this.state.admin ? <NavItem><NavLink tag={Link} className="text-light" to="#">Admin</NavLink></NavItem> : null}
                                        <NavItem>
                                            <LoginLink />
                                        </NavItem>
                                    </ul>
                                </Collapse>
                            </Container>
                        </Navbar>
                    </Collapse>
                </header>
        );
    }

    componentWillUnmount(): void {
        window.removeEventListener("scroll", this.pageScroll);
    }

    componentDidMount(): void {
        window.addEventListener("scroll", this.pageScroll);
        this.checkAuth();
    }

    pageScroll = () => {
        if (this.state.hidden) {
            if (window.scrollY > 50) this.setState({ collapsed: this.state.collapsed, hidden: !this.state.hidden });
        }
        else {
            if (window.scrollY < 20) this.setState({ collapsed: this.state.collapsed, hidden: !this.state.hidden });
        }
    }
}

class LoginLink extends Component<any, { loggedIn: boolean }>
{
    constructor(props: any) {
        super(props);
        this.state = {
            loggedIn: window.localStorage.getItem("id") !== null
        };
        App.webSocket.on("recheckauth", this.recheckAuth.bind(this));
    }

    recheckAuth() {
        if (window.localStorage.getItem("id"))
            this.setState({ loggedIn: true });
        else
            this.setState({ loggedIn: false });
    }

    login() {
        window.open(discordLink + "&state=" + window.localStorage.getItem("websocketID"), "_blank", "status=0,location=0,menubar=0,top=0,left=500,height=700,width=500,scrollbars=0,titlebar=0,minimizable=0");
    }

    logout() {
        var id = window.localStorage.getItem("id");
        if (id)
            fetch("api/v1/users/unauth", {
                method: "DELETE", headers: { "Content-Type": "application/json" }, body: JSON.stringify({
                    guid: id
                })
            }).then(res => {
                if (res.ok) {
                    window.localStorage.removeItem("id");
                    window.localStorage.removeItem("authObj");
                    return Promise.resolve();
                }
            }).then(res => {
                App.webSocket.emit("recheckauth");
            });
    }

    render() {
        return (
            this.state.loggedIn
                ?
                <NavLink className="text-light" onClick={this.logout}>Logout</NavLink>
                :
                <NavLink className="text-light" onClick={this.login}>Login</NavLink>
        );
    }
}