import React, { Component } from "react";
import { Table, Input } from "reactstrap";
import classnames from "classnames";
import "../styles/CheckList.scss";

export class CheckList extends Component<CheckListProps, { sortKey: string, sortAcc: boolean, searchString: string, searchKey: string }> {
    constructor(props: CheckListProps) {
        super(props);
        this.state = { sortAcc: false, sortKey: "", searchKey: "", searchString: "" };
        this.setSort = this.setSort.bind(this);
        this.search = this.search.bind(this);
        this.changeCompleteStateNew = this.changeCompleteStateNew.bind(this);
    }

    setSort(sortKey: string): void {
        this.setState({ sortKey: sortKey });
    }

    firstCharToUpper(input: string): string {
        return input.substring(0, 1).toUpperCase() + input.substring(1);
    }

    getCompletedState(f: CheckListDefault, keys: Dictionary<string, string>, json: any): boolean {
        if (f.uid !== undefined) return json[f.uid];
        return keys.map(t => JSON.parse(JSON.stringify(f))[t] !== undefined ? json[JSON.parse(JSON.stringify(f))[t].uid] !== undefined ? json[JSON.parse(JSON.stringify(f))[t].uid] : true : true).every((t: boolean) => t);
    }

    getUidFromTarget(inp: HTMLElement): string {
        if (inp.nodeName === "TABLE") return "";
        if (inp.dataset.uid === undefined)
            return this.getUidFromTarget(inp.parentElement);
        return inp.dataset.uid;
    }

    changeCompleteStateNew(event: React.MouseEvent<HTMLElement>) {
        var uid = this.getUidFromTarget(event.target as HTMLElement);
        if (uid !== "" && window.localStorage.getItem("id") !== null && window.localStorage.getItem("id") !== undefined && this.props.admin) {
            var rawjson = window.localStorage.getItem(this.props.game);
            if (rawjson === undefined || rawjson === null) rawjson = "{}";
            var json = JSON.parse(rawjson)[this.props.grid];
            if (json === undefined) json = JSON.parse("{}");
            fetch("api/v1/checklist/update", { method: "POST", headers: { "Content-Type": "application/json" }, body: JSON.stringify({ id: window.localStorage.getItem("id"), grid: this.props.grid, game: this.props.game, uid: uid, completed: !json[uid] }) });
        }
        event.preventDefault();
    }

    getText(text: string): Array<any> {
        var arr = text.split("\r").map((t, i) => (<span key={i}>{t}</span>));
        var newArr = arr.flat();
        var f = 0;
        for (let i = 0; i < arr.length - 1; i++) {
            newArr.splice(f + i + 1, 0, (<br key={f + i + arr.length} />));
            f++;
        }
        return newArr;
    }

    search(search: string, key: string){
        this.setState({searchString: search, searchKey: key});
    }

    render(): JSX.Element {
        var rawjson = window.localStorage.getItem(this.props.game);
        if (rawjson === undefined || rawjson === null) rawjson = "{}";
        var json = JSON.parse(rawjson)[this.props.grid];
        if (json === undefined) json = JSON.parse("{}");
        var keys = this.props.keys;
        var table = [];
        console.log(this.props);
        for (let i = 0; i < this.props.data.length; i++) {
            const f = JSON.parse(JSON.stringify(this.props.data[i]));
            if(this.state.searchKey == "" || this.state.searchString == "" || f[this.state.searchKey].toLocaleLowerCase().includes(this.state.searchString))
            table.push((<tr className={classnames({ completed: this.getCompletedState(f, keys, json) })} data-uid={f.uid} key={f.uid !== undefined ? f.uid : this.props.grid + "-" + f.collectiontype}>
                {keys.map(t => {
                    var k = f[t];
                    if (t !== "uid" && typeof (k) === "object") {
                        if (k.value !== undefined) return (<td key={f.uid + "." + t}>{this.getText(k.value)}<span>{this.getText(k.notes)}</span></td>)
                        else return (<td key={f.uid + "." + t} data-uid={k.uid} className={classnames({ completed: json[k.uid] || json[k.uid] === undefined })}>{this.getText(k.notes)}</td>);
                    }
                    if (t !== "uid") return (<td key={f.uid + "." + t}>{typeof (k) === "string" ? this.getText(k) : k}</td>);
                    return null;
                })}
            </tr>));
        }
        return (
            <Table style={{ color: "white" }}>
                <thead>
                    <tr key="head">
                        {keys.map((k, f) => {
                            if (f !== "uid") return (<th key={k}>
                                {f === "id" ? "#" : this.firstCharToUpper(f)}
                                {this.props.admin ?
                                <div>
                                    <br />
                                    <Input onChange={(val) => this.search(val.target.value.toLocaleLowerCase(), k)}/>
                                </div>
                                : null}
                                </th>);
                            return null;
                        })}
                    </tr>
                </thead>
                <tbody onClick={this.changeCompleteStateNew}>
                    {table}
                </tbody>
            </Table>
        );
    }
}

interface CheckListProps {
    data: Array<CheckListDefault>;
    grid: string;
    game: string;
    loc: string;
    keys: Dictionary<string, string>;
    admin: boolean;
}

interface CheckListDefault {
    uid?: string;
    collectiontype?: string;
}

interface IDictionary<K, V> {
    add(key: K, value: V): void;
    remove(key: K): void;
    containsKey(key: K): boolean;
    keys(): K[];
    values(): V[];
    map(cb: (key: K, value: V) => any): any;
}

export class Dictionary<K, V> implements IDictionary<K, V> {
    _keys: K[] = [];
    _values: V[] = [];

    constructor(init: { key: K, value: V }[]) {
        for (var i = 0; i < init.length; i++) {
            if (this.containsKey(init[i].key)) throw new Error("Element already contains key.");
            this._keys.push(init[i].key);
            this._values.push(init[i].value);
        }
    }

    add(key: K, value: V): void {
        if (this.containsKey(key)) throw new Error("Element already contains key.");
        this._keys.push(key);
        this._values.push(value);
    }

    remove(key: K): void {
        var index = this._keys.indexOf(key, 0);
        this._keys.splice(index, 1);
        this._values.splice(index, 1);
    }

    keys(): K[] {
        return this._keys;
    }

    values(): V[] {
        return this._values;
    }

    containsKey(key: K): boolean {
        return this._keys.includes(key);
    }

    toLookup(): IDictionary<K, V> {
        return this;
    }

    map(cb: (key: K, value: V) => any): any {
        var ret = [];
        for (let i = 0; i < this._keys.length; i++) {
            ret.push(cb(this._keys[i], this._values[i]));
        }
        return ret;
    }
}