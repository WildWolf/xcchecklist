import React, { Component, CSSProperties } from "react";
import { Container } from "reactstrap";
import { NavMenu } from "./NavMenu";

export class Layout extends Component<any, { style: CSSProperties }> {
    static displayName = Layout.name;

    constructor(props: any) {
        super(props);
        this.state = { style: {} };
    }

    render() {
        return (
            <div>
                <NavMenu />
                <Container>
                    {this.props.children}
                </Container>
                <div style={this.state.style} />
            </div>
        );
    }

    updateDimensions = () => {
        this.setState(({ style: { height: window.innerHeight, width: window.innerWidth, position: "fixed", background: "rgba(0,0,0,0.3)", zIndex: -1, top: 0 } }) as any);
    }

    componentDidMount(): void {
        window.addEventListener("resize", this.updateDimensions);
        this.updateDimensions();
    }

    componentWillUnmount(): void {
        window.removeEventListener("resize", this.updateDimensions);
    }
}