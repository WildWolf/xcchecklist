﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XCCheckList.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace XCCheckList.Controllers
{
    [Route("api/v1/games")]
    public class GamesController : Controller
    {
        private XCCheckListContext dbContext;

        public GamesController(XCCheckListContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpGet]
        [Route("get")]
        public ActionResult GetGames()
        {
            return Ok(dbContext.Game);
        }
    }
}
