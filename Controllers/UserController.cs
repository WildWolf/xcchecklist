﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using XCCheckList.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace XCCheckList.Controllers
{
    [Route("api/v1/users")]
    public class UserController : Controller
    {
        private XCCheckListContext dbContext;

        public UserController(XCCheckListContext dbContext)
        {
            this.dbContext = dbContext;
        }

        [HttpPost]
        [Route("auth")]
        public ActionResult AuthUser([FromBody] UserControllerPost auth)
        {
            var user = dbContext.User.FirstOrDefault(t => t.Id == auth.Id);
            if (user != null)
            {
                var guid = Guid.NewGuid();
                user.Guid = guid;
                dbContext.SaveChanges();
                return Ok(@$"{{""id"":""{guid}""}}");
            }
            return StatusCode(403);
        }

        [HttpPost]
        [Route("admin")]
        public ActionResult AdminUser([FromBody] UserControllerPost auth)
        {
            if (auth.Guid == null) return Ok(new { b = false });
            var user = dbContext.User.FirstOrDefault(t => t.Guid == auth.Guid);
            return Ok(new { b = user != null });
        }

        [HttpDelete]
        [Route("unauth")]
        public ActionResult UnauthUser([FromBody] UserControllerPost auth)
        {
            var user = dbContext.User.FirstOrDefault(t => t.Guid == auth.Guid);
            if (user != null)
            {
                user.Guid = null;
                dbContext.SaveChanges();
            }
            return Ok();
        }
    }

    public class UserControllerPost
    {
        public ulong? Id { get; set; }

        public string Name { get; set; }

        public string Discriminator { get; set; }
        public Guid? Guid { get; set; }
    }
}