﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using XCCheckList.Models;

namespace XCCheckList.Controllers
{
    [Route("api/v1/checklist")]
    public class CheckListController : Controller
    {
        private XCCheckListContext dbContext;
        private WebSocketManager manager;

        public CheckListController(XCCheckListContext dbContext, WebSocketManager manager)
        {
            this.dbContext = dbContext;
            this.manager = manager;
        }

        [Route("get")]
        public ActionResult GetCheckList([FromQuery] string game, [FromQuery] string grid, [FromQuery] string loc)
        {
            return Ok((from cl in dbContext.CheckList
                       join cln in dbContext.CheckListName on cl.NameId equals cln.Id
                       join g in dbContext.Game on cln.GameId equals g.Id
                       join l in dbContext.Loc on cl.LocId equals l.Id
                       where l.Text == loc && (string.IsNullOrWhiteSpace(grid) || cln.Grid == grid) && g.Name == game
                       select new
                       {
                           cl.Completed,
                           uid = cln.Name,
                           cln.Grid,
                           game = g.Name
                       }).ToList());
        }

        [Route("update")]
        public ActionResult UpdateCheckList([FromBody] CheckListUpdate data)
        {
            if (!dbContext.User.Any(f => f.Guid == data.ID)) return StatusCode(403);
            (from users in dbContext.User
             join locs in dbContext.Loc on users.LocId equals locs.Id
             join checkList in dbContext.CheckList on locs.Id equals checkList.LocId
             join checkListNames in dbContext.CheckListName on checkList.NameId equals checkListNames.Id
             join games in dbContext.Game on checkListNames.GameId equals games.Id
             where users.Guid == data.ID && checkListNames.Grid == data.Grid && games.Name == data.Game && checkListNames.Name == data.UID
             select checkList).First().Completed = data.Completed;
            dbContext.SaveChanges();
            manager.SendMessageToAll(@$"{{""type"":""method"",""method"":""checklistupdate"",""data"":{{""grid"":""{data.Grid}"",""game"":""{data.Game}"",""completed"":{data.Completed.ToString().ToLower()},""uid"":""{data.UID}""}}}}").GetAwaiter().GetResult();
            return Ok();
        }
    }

    public class CheckListUpdate
    {
        public Guid ID;
        public string Grid;
        public string UID;
        public bool Completed;
        public string Game;
    }
}