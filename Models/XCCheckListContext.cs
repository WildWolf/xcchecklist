﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace XCCheckList.Models
{
    public partial class XCCheckListContext : DbContext
    {
        public XCCheckListContext()
        {
        }

        public XCCheckListContext(DbContextOptions<XCCheckListContext> options)
            : base(options)
        {
        }

        public IEnumerable<Loc> GetLocs()
        {
            return Loc.Include(d=> d.CheckList).Include(d => d.User);
        }

        public IEnumerable<CheckList> GetCheckLists()
        {
            return CheckList.Include(d => d.Name).Include(d => d.Name.Game);
        }

        public virtual DbSet<CheckList> CheckList { get; set; }
        public virtual DbSet<CheckListName> CheckListName { get; set; }
        public virtual DbSet<Games> Game { get; set; }
        public virtual DbSet<Loc> Loc { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("DataSource=XCCheckList.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CheckList>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Loc)
                    .WithMany(p => p.CheckList)
                    .HasForeignKey(d => d.LocId);

                entity.HasOne(d => d.Name)
                    .WithOne(p => p.CheckList)
                    .IsRequired();
            });

            modelBuilder.Entity<CheckListName>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Game)
                    .WithMany(p => p.CheckListNames)
                    .HasForeignKey(d => d.GameId);
            });

            modelBuilder.Entity<Games>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasMany(d => d.CheckListNames)
                    .WithOne(p => p.Game)
                    .HasForeignKey(d => d.GameId);
            });

            modelBuilder.Entity<Loc>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.User)
                    .WithOne(p => p.Loc)
                    .IsRequired();
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.HasOne(d => d.Loc)
                    .WithOne(p => p.User)
                    .IsRequired();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
