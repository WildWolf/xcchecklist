﻿using System;
using System.Collections.Generic;

namespace XCCheckList.Models
{
    public class User
    {
        public ulong Id { get; set; }
        public Guid? Guid { get; set; }
        public long LocId { get; set; }
        public virtual Loc Loc { get; set; }
    }
}
