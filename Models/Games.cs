﻿using System;
using System.Collections.Generic;

namespace XCCheckList.Models
{
    public class Games
    {
        public Games()
        {
            CheckListNames = new HashSet<CheckListName>();
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Tabs { get; set; }
        public string Title { get; set; }

        public virtual ICollection<CheckListName> CheckListNames { get; set; }
    }
}
