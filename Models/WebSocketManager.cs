using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Net.WebSockets;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace XCCheckList.Models
{
    public class WebSocketManager{
        private static ConcurrentDictionary<Guid, WebSocket> _sockets = new ConcurrentDictionary<Guid, WebSocket>();

        public WebSocket GetScoketById(Guid id){
            return _sockets.FirstOrDefault(t => t.Key == id).Value;
        }

        public List<WebSocket> GetAll(){
            return _sockets.Select(t => t.Value).ToList();
        }

        public Guid GetId(WebSocket socket)
        {
            return _sockets.FirstOrDefault(t => t.Value == socket).Key;
        }

        public Guid AddSocket(WebSocket socket)
        {
            var id = Guid.NewGuid();
            _sockets.TryAdd(id, socket);
            return id;
        }

        public async Task RemoveSocket(Guid id)
        {
            _sockets.TryRemove(id, out var socket);
            await socket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closed by server", CancellationToken.None);
        }

        public async Task SendMessageToAll(string message)
        {
            foreach (var (guid,socket) in _sockets)
            {
                if (socket.IsOpen())
                    await socket.SendMessageAsync(message);
            }
        }
    }
}