﻿using System;
using System.Collections.Generic;

namespace XCCheckList.Models
{
    public class Loc
    {
        public Loc()
        {
            CheckList = new HashSet<CheckList>();
        }

        public long Id { get; set; }
        public string Text { get; set; }

        public virtual ICollection<CheckList> CheckList { get; set; }
        public virtual User User { get; set; }
    }
}
