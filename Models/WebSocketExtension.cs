﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace XCCheckList.Models
{
    public static class WebSocketExtension
    {
        public static bool IsOpen(this WebSocket socket)
        {
            return socket.State == WebSocketState.Open;
        }

        public static async Task SendMessageAsync(this WebSocket socket, string message)
        {
            if(!socket.IsOpen()) return;

            await socket.SendAsync(new ArraySegment<byte>(Encoding.UTF8.GetBytes(message), 0, message.Length), WebSocketMessageType.Text, true, CancellationToken.None);
        }
    }
}
