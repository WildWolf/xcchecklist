﻿using System;
using System.Collections.Generic;

namespace XCCheckList.Models
{
    public class CheckList
    {
        public long Id { get; set; }
        public long LocId { get; set; }
        public long NameId { get; set; }
        public bool Completed { get; set; }

        public virtual Loc Loc { get; set; }
        public virtual CheckListName Name { get; set; }
    }
}
