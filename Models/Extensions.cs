﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace XCCheckList.Models
{
    public class Extensions
    {
    }

    public static class JsonHandlers
    {
        public static bool TryParse<T>(string input, out T output)
        {
            try
            {
                output = JsonConvert.DeserializeObject<T>(input);
                return true;
            }
            catch
            {
                output = default;
                return false;
            }
        }

        public static bool TryParse(string input, out JArray output)
        {
            try
            {
                output = JArray.Parse(input);
                return true;
            }
            catch
            {
                output = default;
                return false;
            }
        }

        public static bool TryParse(string input, out JObject output)
        {
            try
            {
                output = JObject.Parse(input);
                return true;
            }
            catch
            {
                output = default;
                return false;
            }
        }
    }
}
