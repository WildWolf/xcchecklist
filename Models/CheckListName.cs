﻿using System;
using System.Collections.Generic;

namespace XCCheckList.Models
{
    public class CheckListName
    {

        public long Id { get; set; }
        public long GameId { get; set; }
        public string Name { get; set; }
        public string Grid { get; set; }

        public virtual Games Game { get; set; }
        public virtual CheckList CheckList { get; set; }
    }
}
