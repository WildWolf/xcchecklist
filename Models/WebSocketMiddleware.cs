﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace XCCheckList.Models
{
    public class WebSocketMiddleware
    {
        private readonly RequestDelegate next;
        private readonly WebSocketManager socketManager;
        public WebSocketMiddleware(RequestDelegate next, WebSocketManager socketManager)
        {
            this.next = next;
            this.socketManager = socketManager;
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest || !context.Request.Path.ToString().Contains("websocket"))
            {
                await next.Invoke(context);
                return;
            }
            var socket = await context.WebSockets.AcceptWebSocketAsync();
            var id = socketManager.AddSocket(socket);

#pragma warning disable 4014
            socket.SendMessageAsync(@"{""type"":""method"",""method"":""hello""}");
#pragma warning restore 4014

            var memoryStream = new MemoryStream();

            await Receive(socket, async (result, buffer) =>
            {
                if (result.MessageType == WebSocketMessageType.Close)
                {
                    await socketManager.RemoveSocket(id);
                    return;
                }
                if (!result.EndOfMessage)
                {
                    memoryStream.Write(buffer.Array, buffer.Offset, result.Count);
                }
                else
                {
                    memoryStream.Write(buffer.Array, buffer.Offset, result.Count);
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    using var reader = new StreamReader(memoryStream, Encoding.UTF8);
                    var str = reader.ReadToEnd();

                    if (JsonHandlers.TryParse(str, out JObject jObject))
                    {
                        switch (jObject["type"].ToString())
                        {
                            case "reply":
                                switch (jObject["reply"].ToString())
                                {
                                    case "hello":
                                        await socket.SendMessageAsync(@$"{{""type"":""method"",""method"":""id"",""id"":""{id.ToString()}""}}");
                                        break;
                                }
                                break;
                            case "method":
                                switch (jObject["method"].ToString())
                                {
                                    case "auth":
                                    {
                                        await socketManager.GetScoketById(jObject["data"]["state"].ToObject<Guid>()).SendMessageAsync(@$"{{""type"":""method"",""method"":""auth"",""id"":""{jObject["data"]["id"]}"",""name"":""{jObject["data"]["name"]}"",""discriminator"":""{jObject["data"]["discriminator"]}""}}");
                                        await socket.SendMessageAsync(@$"{{""type"":""reply"",""reply"":""auth""}}");
                                        break;
                                    }
                                    case "textupdate":
                                        await socketManager.SendMessageToAll(@$"{{""type"":""method"",""method"":""textupdate"",""location"":""{jObject["data"]["location"]}""}}");
                                        break;
                                }
                                break;
                        }
                    }

                    memoryStream.Dispose();
                    memoryStream = new MemoryStream();
                }
            });

            memoryStream.Dispose();
        }

        private async Task Receive(WebSocket socket, Action<WebSocketReceiveResult, ArraySegment<byte>> handleMessage)
        {
            var buffer = new ArraySegment<byte>(new byte[4096]);

            while (socket.IsOpen())
            {
                var result = await socket.ReceiveAsync(buffer, CancellationToken.None);

                handleMessage(result, buffer);
            }
        }
    }
}
